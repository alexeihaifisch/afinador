#!/usr/bin/env python
# coding=utf-8
import pyaudio
import matplotlib.pyplot as plt
import numpy as np
from scipy import signal as sgn
import warnings
warnings.simplefilter("ignore", DeprecationWarning)

FORMAT = pyaudio.paInt16
CHANNELS = 2
RATE = 44100
CHUNK = 1024
RECORD_SECONDS = 3
WAVE_OUTPUT_FILENAME = "file.wav"
audio = pyaudio.PyAudio()

def grabarAudio():
     
    # comienza a grabar
    stream = audio.open(format=FORMAT, channels=CHANNELS,
                    rate=RATE, input=True,
                    frames_per_buffer=CHUNK)
    
    print ("recording...")
    frames = []
     
    for i in range(0, int(RATE / CHUNK * RECORD_SECONDS)):
        data = stream.read(CHUNK)
        frames.append(data)
    
    print ("finished recording")
         
    frames = frames[1:]
    
    # termina de grabar
    stream.stop_stream()
    stream.close()
    audio.terminate()
    
    #convierte los bytes de la grabacion a ints
    test = np.array(frames)
    grabacion = np.fromstring(test, np.int16)
    
    #grafica la grabacion
    t = np.linspace(0,RECORD_SECONDS,np.shape(grabacion)[0])
    plt.plot(t,grabacion) 
        
    return grabacion

def quitarRuido(data):
    
    fs = RATE
    #genera un arreglo con el numero de notas tomando como nota central A4=440 Hz
    steps = np.arange(-57, 51)
    #N = orden del filtro
    N = 2
    
    #matriz de datos filtrados, cada fila es un filtro dependiendo de la frecuencia central
    filtrados = []
    
    for i in steps:
        frec, inf, sup = frecuencia(i) #calcula frecuencia central
        w = np.array([inf,sup])/fs 
        b, a = sgn.butter(N,w,btype = 'band') #obten los coeficientes de la funcion de transferencia
        sn = sgn.lfilter(b,a,data) #filtra la señal
        filtrados.append(sn) #agrega
    
    filt = np.array(filtrados)
    
    #indice indica la fila de la matriz filt con el valor mas grande
    indice = np.unravel_index(filt.argmax(), filt.shape)
    print("INDICE: ", indice)
    
    #grafica la señal seleccionada con indice
    t = np.linspace(0,RECORD_SECONDS,np.shape(data)[0])
    plt.plot(t,filt[indice[0]]) 
    plt.show()
    
    #usando indice podemos encontrar que frecuencia tiene mayor valor
    frec, inf, sup = frecuencia(steps[indice[0]])
    print("Tu frecuencia es: ", frec)

    return filt[indice[0]],frec,inf,sup

def frecuencia(i):
    
    #calcula la frecuencia fundamental
    frec = round(440 * np.power(np.power(2,1/12),i), 2)
    
    #calcula dos valores que contengan a la frecuencia central
    inf = round(frec * 0.97, 2)
    sup = round(frec * 1.03, 2)
    
    return frec, inf, sup

#//////
def Frecuencia(octava, nota, accidente):
    """frecuencia obtiene la el valor en Hz dada la octava, nombre de nota y accidente
        Params
        ------
            octava:int
                indica con un entero a que octava (0-8) pertenece la nota             
            nota:str
                indica que tipo de nota es, puede escribirse en ingles o espanol
            accidente:str
                indica si la nota es natural, bemol o sostenida
        Return
        ------
            frec:double
                valor en Hz de la frecuencia
    """
    #1ero se calcula la distancia de la nota respecto a La4
    num_pasos = Pasos(octava, nota, accidente)
    #si no encontro la definicion en el diccionario la frecuencia es cero
    if num_pasos == -100:
        frec = 0
    #Calcula la frecuencia con base en el numero de pasos
    else:
        frec = 440 * pow(pow(2,1/12),num_pasos)
    return frec

def Pasos(octava, nota, accidente):
    """pasos obtiene la distancia de cualquier nota respecto a La4
        Params
        ------
            octava:int
                indica con un entero a que octava (0-8) pertenece la nota             
            nota:str
                indica que tipo de nota es, puede escribirse en ingles o espanol
            accidente:str
                indica si la nota es natural, bemol o sostenida
        Return
        ------
            num_pasos:int
                distancia positiva o negativa de una nota respecto a La4
    """
    #concatena los parametros nota y accidente para buscar la cadena completa en los diccionarios
    nota_real= nota + accidente
    
    #Si la octava es 4 unicamente revisa el diccionario pasos_neutros
    if octava == 4:
        try:
            pasos_neutros   = {'C':-9, 'C#':-8, 'Db':-8, 'D':-7, 'D#':-6, 'Eb':-6, 'E':-5, 'F':-4, 'F#':-3, 'Gb':-3, 'G':-2, 'G#':-1, 'Ab':-1, 'A':0, 'A#':1, 'Bb':1, 'B':2,
                               'Do':-9, 'Do#':-8, 'Reb':-8, 'Re':-7, 'Re#':-6, 'Mib':-6, 'Mi':-5, 'Fa':-4, 'Fa#':-3, 'Solb':-3, 'Sol':-2, 'Sol#':-1, 'Lab':-1, 'La':0, 'La#':1, 'Sib':1, 'Si':2}
            num_pasos = pasos_neutros[nota_real]
        #Si no encuentra la definicion en el diccionario regresa -100
        except:
            num_pasos = -100
    
    #Si la octava es mayor a 4 se cuentan los pasos a la nota similar mas cercana y despues se multiplica por el numero de octavas de diferencia
    if octava > 4:
        try:
            pasos_positivos = {'A':0 , 'A#':1, 'Bb':1, 'B':2, 'C':3, 'C#':4, 'Db':4,'D':5, 'D#':6, 'Eb':6, 'E':7, 'F':8, 'F#':9, 'Gb':9, 'G':10, 'G#':11, 'Ab':11,
                               'La':0 , 'La#':1, 'Sib':1, 'Si':2, 'Do':3, 'Do#':4, 'Reb':4,'Re':5, 'Re#':6, 'Mib':6, 'Mi':7, 'Fa':8, 'Fa#':9, 'Solb':9, 'Sol':10, 'Sol#':11, 'Lab':11}
            num_pasos = pasos_positivos[nota_real]
            distancia_octavas = octava - 5
            num_pasos = (12 * distancia_octavas) + num_pasos
        #Si no encuentra la definicion en el diccionario regresa -100
        except:
            num_pasos = -100
    
    #Si la octava es menor a 4 se cuentan los pasos a la nota similar mas cercana y despues se multiplica por el numero de octavas de diferencia    
    if octava < 4:
        try:
            pasos_negativos = {'A':0, 'Ab':1, 'G#':1, 'G':2, 'Gb':3, 'F#':3, 'F':4, 'E':5, 'Eb':6, 'D#':6, 'D':7, 'Db':8, 'C#':8, 'C':9, 'B':10, 'Bb':11, 'A#':11,
                               'La':0, 'Lab':1, 'Sol#':1, 'Sol':2, 'Solb':3, 'Fa#':3, 'Fa':4, 'Mi':5, 'Mib':6, 'Re#':6, 'Re':7, 'Reb':8, 'Do#':8, 'Do':9, 'Si':10, 'Sib':11, 'La#':11}
            num_pasos = pasos_negativos[nota_real]
            distancia_octavas = 4 - octava
            num_pasos = (-1)*((12 * distancia_octavas) + num_pasos)
        #Si no encuentra la definicion en el diccionario regresa -100
        except:
            num_pasos = -100
        
    return num_pasos
#//////////

def main():
    nota = "Do"
    octava = 6
    accidente = "" #puede ser # o b
    frecuencia_Esperada = Frecuencia(octava,nota,accidente)
    print(frecuencia_Esperada)
    grabacion = grabarAudio()
    sin_ruido,frecuencia_Obtenida, inf, sup = quitarRuido(grabacion)
    print(np.shape(sin_ruido))
    if frecuencia_Esperada < sup and frecuencia_Esperada > inf:
        if frecuencia_Esperada == frecuencia_Obtenida:
            print("Nota afinada")
        if frecuencia_Esperada - frecuencia_Obtenida > 1:
            print("Estas ",(frecuencia_Esperada - frecuencia_Obtenida), " hz por debajo de la nota")
        if frecuencia_Esperada - frecuencia_Obtenida < -1:
            print("Estas ",(frecuencia_Esperada - frecuencia_Obtenida), " hz por arriba de la nota")

if __name__ == "__main__":
    main()
